terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.43.1"
    }
  }
}

resource "openstack_compute_keypair_v2" "test-keypair" {
  name       = "my-keypair"
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "openstack_compute_instance_v2" "ubuntu" {
  count = 2
  name            = "ubuntu_instance-${count.index}"
  provider = openstack
  image_name        = "ubuntu-20.04"
  flavor_name      = "normale"
  key_pair        = "my-keypair"
}

resource "openstack_compute_instance_v2" "centos" {
  count = 2
  name            = "centos_instance-${count.index}"
  provider = openstack
  image_name        = "centos-7"
  flavor_name      = "normale"
  key_pair        = "my-keypair"
}